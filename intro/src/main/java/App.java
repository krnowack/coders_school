public class App {

    public void intro(){
        for(int i=1; i<=100; ++i){
            boolean isMod3 = (i%3 == 0);
            boolean isMod5 = (i%5 == 0);

            if(isMod3 && isMod5){
                System.out.println("FizBuzz");
            }else if(isMod3){
                System.out.println("Fizz");
            }else if(isMod5){
                System.out.println("Buzz");
            }else{
                System.out.println(i);
            }
        }
    }

    public static void main(String[] args) {
        new App().intro();
    }
}
